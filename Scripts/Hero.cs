﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class Hero : MonoBehaviour {

    private Rigidbody2D m_rigidbody;
    public Animator m_animation;
    public GameObject pipeGeneration;

    bool fail = false;
    Queue<GameObject> stack_GamePipe;

    public float posY;
	void Start () {
        stack_GamePipe = new Queue<GameObject>();
        posY = transform.position.y;
        StartCoroutine(spawPipe());
        GameObject.Find("GameOver").GetComponent<Text>().text = "";
    }

    private void Awake()
    {
        m_rigidbody = GetComponent<Rigidbody2D>();
        m_animation = GetComponent<Animator>();
    }

    void Update () {
        m_animation.SetInteger("isState", posY < transform.position.y ? 1:0);
        if (!fail)
        {
            if (Input.GetButtonDown("Jump")) Jump();
            transform.position = Vector3.MoveTowards(transform.position, transform.position + Vector3.right, 3f * Time.deltaTime);
            if (stack_GamePipe.Count != 0)
            {
                if (Vector3.Distance(stack_GamePipe.Peek().transform.position, transform.position) > 20)
                {
                    Destroy(stack_GamePipe.Dequeue());
                }
            }
        }
    }

    void Jump() {
        m_rigidbody.AddForce(Vector2.up*10, ForceMode2D.Impulse);
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "Pipe")
        {
            GameObject.Find("GameOver").GetComponent<Text>().text = "GameOver";

            Transform buttStart = GameObject.Find("ButtonStart").transform;
            buttStart.position = new Vector3(buttStart.position.x, 0, buttStart.position.z);
            GameObject.FindGameObjectWithTag("MainCamera").GetComponent<CameraScript>().isNotEmpty = false;

            fail = true;
        }
    }

    IEnumerator spawPipe()
    {
            float newY;
        GameObject newPipe =Instantiate(pipeGeneration, 
            new Vector3(transform.position.x + 15, Random.Range(-3, 3), 0), pipeGeneration.transform.rotation);
        Vector3 lastPipe = newPipe.transform.position;
        while (true)
        {
            stack_GamePipe.Enqueue(newPipe);
            yield return new WaitForSeconds(3f);


            do
            {
                newY = Random.Range(lastPipe.y - 4, lastPipe.y + 4);
            } while (newY>4 || newY<-4);
            
            newPipe =Instantiate(pipeGeneration, 
                new Vector3(lastPipe.x + 10, newY, 0), pipeGeneration.transform.rotation);
            lastPipe = newPipe.transform.position;
        }
    }

}
