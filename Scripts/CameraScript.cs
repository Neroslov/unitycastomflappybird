﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraScript : MonoBehaviour {

    public Transform m_Hero;
    public float speed = 10f;
    public bool isNotEmpty =false;
	void Start () {
        //m_Hero = GameObject.FindGameObjectWithTag("Player").transform;	
	}
	

	void Update () {
        if (isNotEmpty)
        {
            transform.position = Vector3.MoveTowards(transform.position,
                new Vector3(m_Hero.position.x + 3, transform.position.y, transform.position.z), 2*Vector3.Distance(transform.position, m_Hero.position) * Time.deltaTime);
        }
	}

    public void FindObject(Transform newHero)
    {
            m_Hero = newHero;
            isNotEmpty = true;
    }
}
