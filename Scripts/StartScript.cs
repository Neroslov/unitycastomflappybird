﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class StartScript : MonoBehaviour {

    public Text textButton;
    public CameraScript m_camera;
    public Hero m_hero;
	void Start () {
        textButton = GetComponentInChildren<Text>();
        m_camera = GameObject.FindGameObjectWithTag("MainCamera").GetComponent<CameraScript>();
	}
	
	void Update () {
        
    }

    private void OnMouseDown()
    {
        onNewGame();
    }
    public void onNewGame()
    {
        foreach (GameObject obj in GameObject.FindGameObjectsWithTag("Pipe"))
            Destroy(obj);
        Destroy(GameObject.FindGameObjectWithTag("Player"));
        Hero newHero =Instantiate(m_hero, new Vector3(0, 0, 0), m_hero.transform.rotation);
        m_camera.FindObject(newHero.transform);
        transform.position = new Vector3(transform.position.x, -200, transform.position.z);
    }


}
